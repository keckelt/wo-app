angular.module('starter.controllers', ['starter.utils', 'starter.const'])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $cordovaGeolocation, $localstorage, $httpService) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  var watchOptions = {
    timeout : 15*1000,
    maximumAge: 10*1000,
    enableHighAccuracy: true // may cause errors if true
  };

  ionic.Platform.ready(function(){
      // Code goes here
      $scope.locations = [];
      $scope.print= [];

      $scope.user = $localstorage.getObject('user');

      $scope.print.push("got user data", $scope.user)
      if(!$scope.user || !$scope.user.shortid) {
        $scope.print.push("Create user at server");
        //create user at server
        $httpService.createUser().then(handleUserCreation, function (err) {
          $scope.print.push("error during creation", err);
        });
      }
  });

  $scope.changeName = function(name)
  {
    $scope.print.push("changed name to", name);
    if(!$scope.user || !$scope.user.shortid) {
      $httpService.createUser(name).then(handleUserCreation);
    } else {
      $httpService.updateUser($scope.user).then(handleUserCreation);
    }
  }

  function handleUserCreation(user)
  {
    $scope.print.push("handle user creation")
    //save user
    $localstorage.setObject("user", user);
    //display data
    $scope.user = user;
  }

  $scope.toggleTracking = function(){
    console.log("toggle tracking")
    //see if there is already a watch
    if($scope.watch) {
      stopTracking();
    }
    else {
      startTracking();
    }
  }

  function startTracking()
  {
    console.log("Starting to track");

    $scope.watch = $cordovaGeolocation.watchPosition(watchOptions);
    $scope.watch.then(
      null,
      function(err) {
        // error
        console.log(err);
        $scope.locations.push(JSON.stringify(err));
      },
      savePosition
    );
  }

  function stopTracking()
  {
    console.log("Stop tracking");

    if($scope.watch)
    {
      $scope.watch.clearWatch();
      $scope.watch = null;
    }
  }

    function savePosition(position)
    {
      if(position.coords.accuracy > 500)
      {
        console.log("forget accuracy", position.coords.accuracy);
        return;
      }

      if($scope.locations.length >=1)
      {
        var oldTime = $scope.locations[$scope.locations.length-1].timestamp;
        var newTime = position.timestamp;

        if((newTime - oldTime) <= 5000)
        { // Forget Locations
          console.log('forget loaction, too new');
          return;
        }
      }

      var pos = {
        accuracy : position.coords.accuracy,
        altitude: position.coords.altitude,
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
        speed: position.coords.speed,
        timestamp: position.timestamp
      };

      $scope.locations.push(pos);
      $scope.print.push(
        new Date(pos.timestamp).toLocaleTimeString()+" - "+
        "Acc: "+Math.floor(pos.accuracy) +
        "| Lat: "+pos.latitude +
        " Long: "+pos.longitude
      );
      console.log("got position", pos);
      $httpService.postLocation(pos, $scope.user.shortid).then(function(response) {
        $scope.print.push("Upload successful");
      }, function(err) {
        console.log("post err", err);
        $scope.print.push("Upload error" + err);
      });
    }
})
