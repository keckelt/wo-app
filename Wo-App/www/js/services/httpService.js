angular.module('starter.utils')

.factory('$httpService', ['$window', '$http', '$q', 'API_KEYS', function($window, $http, $q, API_KEYS) {
  return {

    createUser : function(name) {
      var defer = $q.defer();
      var req = {
        method: 'POST',
        url: "http://wo.eckelt.me:1337/user",
        headers: {
          "Authorization": API_KEYS.WO
        },
        data: {
          "name": name
        }
      }

      $http(req).then(function(response) {

        /*var data = response.data,
        status = response.status,
        header = response.header,
        config = response.config;*/
        // success handler
        defer.resolve(response.data);
      }, function(errorResponse) {

        /*  var data = response.data,
        status = response.status,
        header = response.header,
        config = response.config; */
        // error handler
        defer.reject(errorResponse);
      });

      return defer.promise;
    },

    updateUser : function(user) {
      var defer = $q.defer();
      var req = {
        method: 'PUT',
        url: "http://wo.eckelt.me:1337/user/"+user.shortid,
        headers: {
          "Authorization": API_KEYS.WO
        },
        data: {
          "name": user.name
        }
      }

      $http(req).then(function(response) {

        /*var data = response.data,
        status = response.status,
        header = response.header,
        config = response.config;*/
        // success handler
        defer.resolve(response.data);
      }, function(errorResponse) {

        /*  var data = response.data,
        status = response.status,
        header = response.header,
        config = response.config; */
        // error handler
        defer.reject(errorResponse);
      });

      return defer.promise;
    },

    postLocation: function(pos, id) {
      var defer = $q.defer();
      var req = {
        method: 'POST',
        url: "http://wo.eckelt.me:1337/location",
        headers: {
          "Authorization": API_KEYS.WO
        },
        data: {
          "latitude": pos.latitude,
          "longitude": pos.longitude,
          "user": id
        }
      }

      console.log("request data", req.data);

      $http(req).then(function(response) {

        /*var data = response.data,
        status = response.status,
        header = response.header,
        config = response.config;*/
        // success handler
        defer.resolve(response);
      }, function(errorResponse) {

        /*  var data = response.data,
        status = response.status,
        header = response.header,
        config = response.config; */
        // error handler
        defer.reject(errorResponse);
      });

      return defer.promise;
    }
  }
}]);
