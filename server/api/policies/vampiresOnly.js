/**
 * api/policies/authenticated.js
 *
 * This example shows how to use the HTTP Basic authentication strategy using the passport-http module.
 * Other strategies (Digest, OAuth, OAuth2, etc) can be similarly implemented.
 *
**/

module.exports = function vampiresOnly(req, res, ok) {
  // -----------------------------------------------------------------------
  // Authentification

  console.log("do auth");

  var auth = {login: 'dragula', password: 'I_dr1nk_Y0ur_B100D'}; // change this


  var reqAuth = req.headers.authorization || '';
  var reqSplit = reqAuth.split(' ')[1]
  var b64auth = reqSplit || '';

  var credentials = new Buffer(b64auth, 'base64').toString().split(':');
  var login = credentials[0];
  var password = credentials[1];

  console.log("data", login, password, auth);
  // Verify login and password are set and correct
  if (!login || !password || login !== auth.login || password !== auth.password) {
    res.set('WWW-Authenticate', 'Basic realm="nope"') // change this
    res.status(401).send('You shall not pass.') // custom message
    return
  }

  return ok();
};
