/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    show: function (req, res) {
        sails.log.debug("Show for user", req.param('userId'));
        User.findOne({shortid: req.param('userId')}).populate('locations').exec(function (err, user) {
            if(err) return res.serverError(err);

            if(!user) return res.notFound("No user with id: \""+req.param('userId')+"\"");

            sails.log.info("Display view for user: ", user);
            return res.view('user', { TROLOLO: user, title: 'bla' });
        });
    }

};

