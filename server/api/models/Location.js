/**
 * Location.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

      latitude: {
          type : 'String',
          required: true
      },

      longitude: {
          type: 'String',
          required: true
      },

      user: {
          model: 'user',
          required: true
      }
  },

    beforeCreate: function(params, cb) {
        User.findOrCreate({shortid: params.user}).exec(function (err, user) {
            if(err) return cb(err);

            params.user = user.shortid;
            cb();
        });
    }
};

