/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
var shortid = require('shortid');

module.exports = {

    autoPK: false,

    attributes: {
        shortid: {
            type: 'string',
            primaryKey: true,
            unique: true,
            required: true
        },

        name: {
            type: 'string'
        },

        locations: {
            collection: 'location',
            via: 'user'
        }
    },

    // Lifecycle Callbacks
    beforeValidate: function (values, cb) {

      if(!values.shortid || values.shortid == "") //null or empty
      {
        values.shortid =  shortid.generate();
      }

      cb();
    }
};
