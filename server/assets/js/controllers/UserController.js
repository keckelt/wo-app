angular.module('testApp').controller('UserCtrl', ['$scope', '$routeParams', '$location', 'UserService', '$interval', 'LocationService', function($scope, $routeParams, $location, UserService, $interval, LocationService) {

  //init location array to push new locations on it
  $scope.locations = [];

  console.log("UserController recieved id: ", $routeParams.userId);

  //Draw Map
  createMap();

  //Get User Data (once is enough)
  UserService.getUser($routeParams.userId).then(function(response) {
    $scope.user = response.data;
  }, function(err) {
    //Go back to home
    $location.path("/"+err.status);
  });


  //Get Locations every few seconds and then draw them on the map
  $interval(function () {
    LocationService.getLocations($scope.user.shortid, $scope.locations.length).then(function (response) {
      console.log("locations recieved", response.data);
      var newLocations = response.data;
      $scope.locations = $scope.locations.concat(newLocations);
      console.log("new location array: ", $scope.locations)
      if(newLocations && newLocations.length > 0) {
        addMapMarkers(newLocations);
      }
    }, function (err) {
      console.log("error getting location", err);
    });
  }, 3333);


  //http://openlayers.org/en/v3.14.2/examples/feature-move-animation.html?q=animation
  function createMap(locations) {
    $scope.mapMarkers = new ol.source.Vector({});

    var vectorLayer = new ol.layer.Vector({
     source: $scope.mapMarkers,
     style: new ol.style.Style({
        image: new ol.style.Circle({
          radius: 10,
          snapToPixel: false,
          fill: new ol.style.Fill({color: [100,200,200,0.5]}),
          stroke: new ol.style.Stroke({
            color: [100,200,200,0.9], width: 2
          })
        }),
        stroke: new ol.style.Stroke({
          width: 5,
          color: [100,200,200,0.9]
        })
      })
   });

   $scope.map = new ol.Map({
     target: 'map',
     loadTilesWhileAnimating: true,
     view: new ol.View({
       center: ol.proj.fromLonLat([14.282355, 48.311300]),
       zoom: 15,
       minZoom: 2,
       maxZoom: 19
     }),
     layers: [
       new ol.layer.Tile({
         source: new ol.source.MapQuest({layer: 'osm'})
       }),
       vectorLayer
     ]
   });
  }

  function addMapMarkers(locations) {

    var addRouteFeature = false;

    if(!$scope.route) {
      $scope.route = new ol.geom.LineString();
      addRouteFeature = true;
    }

    var mapPoints = [];
    locations.forEach(function (location){
      mapPoints.push(
        new ol.Feature({
          type: 'point',
          geometry: new ol.geom.Point(ol.proj.fromLonLat([parseFloat(location.longitude), parseFloat(location.latitude)]))
        })
      );

      $scope.route.appendCoordinate(
        ol.proj.fromLonLat([parseFloat(location.longitude), parseFloat(location.latitude)])
      );
    });

    if(addRouteFeature) {
      $scope.mapMarkers.addFeature(new ol.Feature($scope.route));
    }

    $scope.mapMarkers.addFeatures(mapPoints);

    var lastLocation = locations[locations.length-1];
    $scope.map.getView().setCenter(ol.proj.fromLonLat([parseFloat(lastLocation.longitude), parseFloat(lastLocation.latitude)]));
  }
}])
