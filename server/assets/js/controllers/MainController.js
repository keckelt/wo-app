'use strict';

  // Here we attach this controller to our testApp module
  angular.module('testApp')

    // The controller function let's us give our controller a name: MainCtrl
    // We'll then pass an anonymous function to serve as the controller itself.
    .controller('MainCtrl', ['$scope', '$routeParams', function($scope, $routeParams) {

      $scope.errCode = $routeParams.errCode;
      console.log("callled with error code: ", $scope.errCode);

      if($scope.errCode == "404")
      {
        $scope.errMessage = "Mit dieser ID konnte kein User gefunden werden";
      }

    }]);
