angular.module('testApp').service('UserService', function($http, $q) {
  return {
    'getUser': function(userId) {
      var defer = $q.defer();

      $http.get('/user/'+userId).then(function(response) {
          var data = response.data,
              status = response.status,
              header = response.header,
              config = response.config;
          // success handler
          defer.resolve(response);
      }, function(response) {
          var data = response.data,
              status = response.status,
              header = response.header,
              config = response.config;
          // error handler
          defer.reject(response);
      });

      return defer.promise;
    }
  }
});
