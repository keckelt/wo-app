angular.module('testApp').service('LocationService', function($http, $q) {
  return {
    'getLocations': function(userId, numToSkip) {
      var defer = $q.defer();

      if(!numToSkip) numToSkip = 0;

      console.log("Get Locations for "+userId+". Skip: "+numToSkip);

      $http.get('location/?where={"user":"' + userId + '"}' + '&skip='+numToSkip).then(function(response) {
          var data = response.data,
              status = response.status,
              header = response.header,
              config = response.config;
          // success handler
          defer.resolve(response);
      }, function(response) {
          var data = response.data,
              status = response.status,
              header = response.header,
              config = response.config;
          // error handler
          defer.reject(response);
      });

      return defer.promise;
    }
  }
});
